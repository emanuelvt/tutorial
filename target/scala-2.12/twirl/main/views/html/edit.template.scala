
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object edit extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Book],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(bookForm: Form[Book]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*2.2*/import helper._


Seq[Any](format.raw/*1.24*/("""
"""),_display_(/*3.2*/layout("Editar libro")/*3.24*/{_display_(Seq[Any](format.raw/*3.25*/("""
	"""),format.raw/*4.2*/("""<h1>Edit book</h1>
		"""),_display_(/*5.4*/helper/*5.10*/.form(action = routes.BooksController.update())/*5.57*/{_display_(Seq[Any](format.raw/*5.58*/("""
			"""),_display_(/*6.5*/helper/*6.11*/.inputText(bookForm("id"))),format.raw/*6.37*/("""
			"""),_display_(/*7.5*/helper/*7.11*/.inputText(bookForm("title"))),format.raw/*7.40*/("""
			"""),_display_(/*8.5*/helper/*8.11*/.inputText(bookForm("price"))),format.raw/*8.40*/("""
			"""),_display_(/*9.5*/helper/*9.11*/.inputText(bookForm("author"))),format.raw/*9.41*/("""
			
			  """),_display_(/*11.7*/CSRF/*11.11*/.formField),format.raw/*11.21*/("""
			"""),format.raw/*12.4*/("""<input type="submit" value="Editar libro">
		
		""")))}),format.raw/*14.4*/("""

""")))}),format.raw/*16.2*/("""

"""))
      }
    }
  }

  def render(bookForm:Form[Book]): play.twirl.api.HtmlFormat.Appendable = apply(bookForm)

  def f:((Form[Book]) => play.twirl.api.HtmlFormat.Appendable) = (bookForm) => apply(bookForm)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Aug 28 07:27:59 CDT 2017
                  SOURCE: C:/Users/vasco/Downloads/play-java-starter-example/play-java-starter-example/tutorial/app/views/edit.scala.html
                  HASH: ce75cb7c88239cdd2312f3b4c4f8c5689c77021d
                  MATRIX: 951->1|1046->26|1091->23|1119->44|1149->66|1187->67|1216->70|1264->93|1278->99|1333->146|1371->147|1402->153|1416->159|1462->185|1493->191|1507->197|1556->226|1587->232|1601->238|1650->267|1681->273|1695->279|1745->309|1784->322|1797->326|1828->336|1860->341|1941->392|1976->397
                  LINES: 28->1|31->2|34->1|35->3|35->3|35->3|36->4|37->5|37->5|37->5|37->5|38->6|38->6|38->6|39->7|39->7|39->7|40->8|40->8|40->8|41->9|41->9|41->9|43->11|43->11|43->11|44->12|46->14|48->16
                  -- GENERATED --
              */
          