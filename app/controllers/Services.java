package controllers;
import play.mvc.*;
import views.html.*;

public class Services extends Controller{
	
	

	public Result list() {
		return TODO;
	}
	
	public Result info() {
		return ok(info.render());
	}
	
	public Result about() {
		return ok(about.render());
	}
	
	public Result bienvenido(String name, String lastName) {
		return ok(bienvenido.render(name, lastName));
	}
	
}
