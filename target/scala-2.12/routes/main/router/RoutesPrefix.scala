
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/vasco/Downloads/play-java-starter-example/play-java-starter-example/tutorial/conf/routes
// @DATE:Sun Aug 27 11:24:10 CDT 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
