package com.aztlansoft.tutorial.models;

import java.sql.Timestamp;
import java.util.*;
import javax.persistence.*;

import io.ebean.*;
import io.ebean.annotation.NotNull;
import io.ebean.annotation.WhenCreated;
import io.ebean.annotation.WhenModified;

@MappedSuperclass
public abstract class BaseModel extends Model {
  protected static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  protected Long id;

  @NotNull
  @Column(name = "active", nullable = false, columnDefinition = "boolean default true")
  protected boolean active = true;

  @WhenCreated
  protected Timestamp created;

  @WhenModified
  protected Timestamp modified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public Timestamp getModified() {
    return this.modified;
  }

  public Timestamp getCreated() {
    return this.created;
  }
}
