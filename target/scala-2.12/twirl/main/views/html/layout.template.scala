
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object layout extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title:String)(body: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.28*/("""
"""),format.raw/*2.1*/("""<html>
	<head>
		<title>"""),_display_(/*4.11*/title),format.raw/*4.16*/("""</title>
	</head>
	<body>
		"""),_display_(/*7.4*/body),format.raw/*7.8*/("""
	

"""),format.raw/*10.1*/("""</body>
</html>"""))
      }
    }
  }

  def render(title:String,body:Html): play.twirl.api.HtmlFormat.Appendable = apply(title)(body)

  def f:((String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title) => (body) => apply(title)(body)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sun Aug 27 18:08:04 CDT 2017
                  SOURCE: C:/Users/vasco/Downloads/play-java-starter-example/play-java-starter-example/tutorial/app/views/layout.scala.html
                  HASH: 250febddccc5ffaaa3006edca524b9a166f231ab
                  MATRIX: 954->1|1075->27|1103->29|1156->56|1181->61|1238->93|1261->97|1295->104
                  LINES: 28->1|33->1|34->2|36->4|36->4|39->7|39->7|42->10
                  -- GENERATED --
              */
          