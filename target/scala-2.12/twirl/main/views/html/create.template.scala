
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object create extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Book],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(bookForm: Form[Book]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*2.2*/import helper._


Seq[Any](format.raw/*1.24*/("""
"""),_display_(/*3.2*/layout("Agregar libro")/*3.25*/{_display_(Seq[Any](format.raw/*3.26*/("""
"""),format.raw/*4.1*/("""<h1>Agregar libro</h1>
		"""),_display_(/*5.4*/helper/*5.10*/.form(action = routes.BooksController.save())/*5.55*/{_display_(Seq[Any](format.raw/*5.56*/("""
			"""),_display_(/*6.5*/helper/*6.11*/.inputText(bookForm("id"))),format.raw/*6.37*/("""
			"""),_display_(/*7.5*/helper/*7.11*/.inputText(bookForm("title"))),format.raw/*7.40*/("""
			"""),_display_(/*8.5*/helper/*8.11*/.inputText(bookForm("price"))),format.raw/*8.40*/("""
			"""),_display_(/*9.5*/helper/*9.11*/.inputText(bookForm("author"))),format.raw/*9.41*/("""
			
			  """),_display_(/*11.7*/CSRF/*11.11*/.formField),format.raw/*11.21*/("""
			"""),format.raw/*12.4*/("""<input type="submit" value="Agregar libro">
		
		""")))}),format.raw/*14.4*/("""
	

""")))}),format.raw/*17.2*/("""
"""))
      }
    }
  }

  def render(bookForm:Form[Book]): play.twirl.api.HtmlFormat.Appendable = apply(bookForm)

  def f:((Form[Book]) => play.twirl.api.HtmlFormat.Appendable) = (bookForm) => apply(bookForm)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Aug 28 07:31:15 CDT 2017
                  SOURCE: C:/Users/vasco/Downloads/play-java-starter-example/play-java-starter-example/tutorial/app/views/create.scala.html
                  HASH: 4c90f8f6df692c8db24fd6b9fe607814636bc961
                  MATRIX: 953->1|1048->26|1093->23|1121->44|1152->67|1190->68|1218->70|1270->97|1284->103|1337->148|1375->149|1406->155|1420->161|1466->187|1497->193|1511->199|1560->228|1591->234|1605->240|1654->269|1685->275|1699->281|1749->311|1788->324|1801->328|1832->338|1864->343|1946->395|1984->403
                  LINES: 28->1|31->2|34->1|35->3|35->3|35->3|36->4|37->5|37->5|37->5|37->5|38->6|38->6|38->6|39->7|39->7|39->7|40->8|40->8|40->8|41->9|41->9|41->9|43->11|43->11|43->11|44->12|46->14|49->17
                  -- GENERATED --
              */
          