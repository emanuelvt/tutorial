
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object indexOfBooks extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Set[Book],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(books : Set[Book]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.21*/("""
"""),_display_(/*2.2*/layout("Todos los libros")/*2.28*/{_display_(Seq[Any](format.raw/*2.29*/("""

		"""),format.raw/*4.3*/("""<h1>Todos los libros</h1>
	
	"""),_display_(/*6.3*/for(book <- books) yield /*6.21*/{_display_(Seq[Any](format.raw/*6.22*/("""
		"""),format.raw/*7.3*/("""<a href=""""),_display_(/*7.13*/routes/*7.19*/.BooksController.show(book.id)),format.raw/*7.49*/("""">"""),_display_(/*7.52*/book/*7.56*/.title),format.raw/*7.62*/("""</a>
		<p>Precio: """),_display_(/*8.15*/book/*8.19*/.price),format.raw/*8.25*/("""</p>
		<p>Autor: """),_display_(/*9.14*/book/*9.18*/.author),format.raw/*9.25*/("""</p>
		
	""")))}),format.raw/*11.3*/("""
	
	"""),format.raw/*13.2*/("""<a href=""""),_display_(/*13.12*/routes/*13.18*/.BooksController.create()),format.raw/*13.43*/("""">Agregar libro</a>


""")))}))
      }
    }
  }

  def render(books:Set[Book]): play.twirl.api.HtmlFormat.Appendable = apply(books)

  def f:((Set[Book]) => play.twirl.api.HtmlFormat.Appendable) = (books) => apply(books)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Aug 28 07:27:59 CDT 2017
                  SOURCE: C:/Users/vasco/Downloads/play-java-starter-example/play-java-starter-example/tutorial/app/views/indexOfBooks.scala.html
                  HASH: 826028e1920cc13a93ee4485847acfed44331e63
                  MATRIX: 958->1|1072->20|1100->23|1134->49|1172->50|1204->56|1261->88|1294->106|1332->107|1362->111|1398->121|1412->127|1462->157|1491->160|1503->164|1529->170|1575->190|1587->194|1613->200|1658->219|1670->223|1697->230|1739->242|1772->248|1809->258|1824->264|1870->289
                  LINES: 28->1|33->1|34->2|34->2|34->2|36->4|38->6|38->6|38->6|39->7|39->7|39->7|39->7|39->7|39->7|39->7|40->8|40->8|40->8|41->9|41->9|41->9|43->11|45->13|45->13|45->13|45->13
                  -- GENERATED --
              */
          