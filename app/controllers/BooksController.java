package controllers;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;

import java.util.List;
import java.util.Set;

import models.Book;
import views.html.*;
import javax.inject.Inject;



public class BooksController extends Controller{
	
	@Inject 
	FormFactory formFactory;
	
	// para todos los libros
	public Result index() {
		Set<Book> books = Book.allBooks();
		
		return ok(indexOfBooks.render(books));
	}
	
	// para crear un libro
	public Result create() {
		Form<Book> bookForm = formFactory.form(Book.class);
		return ok(create.render(bookForm));
	}
	
	// para guardar un libro
	public Result save() {
		Form<Book> bookForm = formFactory.form(Book.class).bindFromRequest();
		Book book = bookForm.get();
		Book.add(book);
		return redirect(routes.BooksController.index());
	}
	
	// para editar un libro. Necesario saber la id del libro que será editado.
	public Result edit(Integer id) {
		Book book = Book.findById(id);
		if (book == null) {
			return notFound("Sorry, book not found");
		}
		Form<Book> bookForm = formFactory.form(Book.class).fill(book);
		return ok(edit.render(bookForm));
	}
	
	// para actualizar un libro
	public Result update() {
		Book book = formFactory.form(Book.class).bindFromRequest().get();
		Book oldBook = Book.findById(book.id);
		if(oldBook == null) {
			return notFound("Book Not Found");
		}
		 oldBook.title = book.title;
		 oldBook.author = book.author;
		 oldBook.price = book.price;
		
		return redirect(routes.BooksController.index());
	}
	
	// para borrar un libro
	public Result destroy(Integer id) {
		Book book = Book.findById(id);
		if (book == null) {
			return notFound("Book not found");
		}
		Book.remove(book);
		return redirect(routes.BooksController.index());
		
		
	}
	
	// para mostrar detalles de un libro
	public Result show(Integer id) {
		Book book = Book.findById(id);
		if(book==null) {
			return notFound("Book Not Found!");
		}
	return ok(show.render(book));
	}
}