# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table post (
  name                          varchar(255),
  done                          boolean default false not null,
  due_date                      timestamp
);


# --- !Downs

drop table if exists post;

