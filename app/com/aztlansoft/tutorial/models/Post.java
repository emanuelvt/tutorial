package com.aztlansoft.tutorial.models;

import javax.persistence.Entity;

import java.util.*;
import io.ebean.*;
import play.data.format.*;
import play.data.validation.Constraints;

@Entity
public class Post extends Model {

    @Constraints.Required
    public String name;

    public boolean done;

    @Formats.DateTime(pattern="dd/MM/yyyy")
    public Date dueDate = new Date();

    public static final Finder<Long, Post> find = new Finder<>(Post.class);
}
