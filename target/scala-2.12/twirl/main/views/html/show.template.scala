
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object show extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Book,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(book:Book):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.13*/("""
"""),_display_(/*2.2*/layout(book.title)/*2.20*/{_display_(Seq[Any](format.raw/*2.21*/("""

	"""),format.raw/*4.2*/("""<h1>"""),_display_(/*4.7*/book/*4.11*/.title),format.raw/*4.17*/("""</h1>
		<p>Precio: """),_display_(/*5.15*/book/*5.19*/.price),format.raw/*5.25*/(""" """),format.raw/*5.26*/("""$</p>
		<p>Autor: """),_display_(/*6.14*/book/*6.18*/.author),format.raw/*6.25*/("""</p>
		
		<a href=""""),_display_(/*8.13*/routes/*8.19*/.BooksController.edit(book.id)),format.raw/*8.49*/("""">Editar</a>
		<a href=""""),_display_(/*9.13*/routes/*9.19*/.BooksController.destroy(book.id)),format.raw/*9.52*/("""">Eliminar</a>

""")))}),format.raw/*11.2*/("""
"""))
      }
    }
  }

  def render(book:Book): play.twirl.api.HtmlFormat.Appendable = apply(book)

  def f:((Book) => play.twirl.api.HtmlFormat.Appendable) = (book) => apply(book)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Aug 28 07:27:59 CDT 2017
                  SOURCE: C:/Users/vasco/Downloads/play-java-starter-example/play-java-starter-example/tutorial/app/views/show.scala.html
                  HASH: 934ff1919a128d557e157db1d31b860b6de68aed
                  MATRIX: 945->1|1051->12|1079->15|1105->33|1143->34|1174->39|1204->44|1216->48|1242->54|1289->75|1301->79|1327->85|1355->86|1401->106|1413->110|1440->117|1488->139|1502->145|1552->175|1604->201|1618->207|1671->240|1720->259
                  LINES: 28->1|33->1|34->2|34->2|34->2|36->4|36->4|36->4|36->4|37->5|37->5|37->5|37->5|38->6|38->6|38->6|40->8|40->8|40->8|41->9|41->9|41->9|43->11
                  -- GENERATED --
              */
          