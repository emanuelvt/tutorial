
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object about extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*1.2*/main("About Us")/*1.18*/{_display_(Seq[Any](format.raw/*1.19*/("""
"""),format.raw/*2.1*/("""<h1>This is all about us!</h1>
""")))}),format.raw/*3.2*/("""
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sun Aug 27 06:06:14 CDT 2017
                  SOURCE: C:/Users/vasco/Downloads/play-java-starter-example/play-java-starter-example/tutorial/app/views/about.scala.html
                  HASH: 99e67068f54c06e8f62ff87a771ceb5208c8cd9e
                  MATRIX: 1030->1|1054->17|1092->18|1120->20|1182->53
                  LINES: 33->1|33->1|33->1|34->2|35->3
                  -- GENERATED --
              */
          