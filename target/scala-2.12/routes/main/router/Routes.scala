
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/vasco/Downloads/play-java-starter-example/play-java-starter-example/tutorial/conf/routes
// @DATE:Sun Aug 27 11:24:10 CDT 2017

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  HomeController_1: controllers.HomeController,
  // @LINE:8
  CountController_0: controllers.CountController,
  // @LINE:10
  AsyncController_3: controllers.AsyncController,
  // @LINE:13
  Assets_5: controllers.Assets,
  // @LINE:16
  Services_4: controllers.Services,
  // @LINE:23
  BooksController_2: controllers.BooksController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    HomeController_1: controllers.HomeController,
    // @LINE:8
    CountController_0: controllers.CountController,
    // @LINE:10
    AsyncController_3: controllers.AsyncController,
    // @LINE:13
    Assets_5: controllers.Assets,
    // @LINE:16
    Services_4: controllers.Services,
    // @LINE:23
    BooksController_2: controllers.BooksController
  ) = this(errorHandler, HomeController_1, CountController_0, AsyncController_3, Assets_5, Services_4, BooksController_2, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_1, CountController_0, AsyncController_3, Assets_5, Services_4, BooksController_2, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """count""", """controllers.CountController.count"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """message""", """controllers.AsyncController.message"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """services""", """controllers.Services.list"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """services/new""", """controllers.Services.info"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """services/about""", """controllers.Services.about"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """services/bienvenido/""" + "$" + """name<[^/]+>/""" + "$" + """lastName<[^/]+>""", """controllers.Services.bienvenido(name:String, lastName:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """books""", """controllers.BooksController.index()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """books/create""", """controllers.BooksController.create()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """books/""" + "$" + """id<[^/]+>""", """controllers.BooksController.show(id:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """books/edit/""" + "$" + """id<[^/]+>""", """controllers.BooksController.edit(id:Integer)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """books/edit""", """controllers.BooksController.update()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """books/create/""", """controllers.BooksController.save()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """books/delete/""" + "$" + """id<[^/]+>""", """controllers.BooksController.destroy(id:Integer)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_1.index,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ An example controller showing a sample home page""",
      Seq()
    )
  )

  // @LINE:8
  private[this] lazy val controllers_CountController_count1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("count")))
  )
  private[this] lazy val controllers_CountController_count1_invoker = createInvoker(
    CountController_0.count,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CountController",
      "count",
      Nil,
      "GET",
      this.prefix + """count""",
      """ An example controller showing how to use dependency injection""",
      Seq()
    )
  )

  // @LINE:10
  private[this] lazy val controllers_AsyncController_message2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("message")))
  )
  private[this] lazy val controllers_AsyncController_message2_invoker = createInvoker(
    AsyncController_3.message,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AsyncController",
      "message",
      Nil,
      "GET",
      this.prefix + """message""",
      """ An example controller showing how to write asynchronous code""",
      Seq()
    )
  )

  // @LINE:13
  private[this] lazy val controllers_Assets_versioned3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned3_invoker = createInvoker(
    Assets_5.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )

  // @LINE:16
  private[this] lazy val controllers_Services_list4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("services")))
  )
  private[this] lazy val controllers_Services_list4_invoker = createInvoker(
    Services_4.list,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Services",
      "list",
      Nil,
      "GET",
      this.prefix + """services""",
      """""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_Services_info5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("services/new")))
  )
  private[this] lazy val controllers_Services_info5_invoker = createInvoker(
    Services_4.info,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Services",
      "info",
      Nil,
      "GET",
      this.prefix + """services/new""",
      """""",
      Seq()
    )
  )

  // @LINE:18
  private[this] lazy val controllers_Services_about6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("services/about")))
  )
  private[this] lazy val controllers_Services_about6_invoker = createInvoker(
    Services_4.about,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Services",
      "about",
      Nil,
      "GET",
      this.prefix + """services/about""",
      """""",
      Seq()
    )
  )

  // @LINE:19
  private[this] lazy val controllers_Services_bienvenido7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("services/bienvenido/"), DynamicPart("name", """[^/]+""",true), StaticPart("/"), DynamicPart("lastName", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Services_bienvenido7_invoker = createInvoker(
    Services_4.bienvenido(fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Services",
      "bienvenido",
      Seq(classOf[String], classOf[String]),
      "GET",
      this.prefix + """services/bienvenido/""" + "$" + """name<[^/]+>/""" + "$" + """lastName<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:23
  private[this] lazy val controllers_BooksController_index8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("books")))
  )
  private[this] lazy val controllers_BooksController_index8_invoker = createInvoker(
    BooksController_2.index(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.BooksController",
      "index",
      Nil,
      "GET",
      this.prefix + """books""",
      """Mostrar todos los libros.""",
      Seq()
    )
  )

  // @LINE:26
  private[this] lazy val controllers_BooksController_create9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("books/create")))
  )
  private[this] lazy val controllers_BooksController_create9_invoker = createInvoker(
    BooksController_2.create(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.BooksController",
      "create",
      Nil,
      "GET",
      this.prefix + """books/create""",
      """Mostrar las opciones para crear un libro.""",
      Seq()
    )
  )

  // @LINE:29
  private[this] lazy val controllers_BooksController_show10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("books/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_BooksController_show10_invoker = createInvoker(
    BooksController_2.show(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.BooksController",
      "show",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """books/""" + "$" + """id<[^/]+>""",
      """Mostrar un libro en específico.""",
      Seq()
    )
  )

  // @LINE:32
  private[this] lazy val controllers_BooksController_edit11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("books/edit/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_BooksController_edit11_invoker = createInvoker(
    BooksController_2.edit(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.BooksController",
      "edit",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """books/edit/""" + "$" + """id<[^/]+>""",
      """Mostrar las opciones para editar un libro.""",
      Seq()
    )
  )

  // @LINE:35
  private[this] lazy val controllers_BooksController_update12_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("books/edit")))
  )
  private[this] lazy val controllers_BooksController_update12_invoker = createInvoker(
    BooksController_2.update(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.BooksController",
      "update",
      Nil,
      "POST",
      this.prefix + """books/edit""",
      """Editar un libro.""",
      Seq()
    )
  )

  // @LINE:38
  private[this] lazy val controllers_BooksController_save13_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("books/create/")))
  )
  private[this] lazy val controllers_BooksController_save13_invoker = createInvoker(
    BooksController_2.save(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.BooksController",
      "save",
      Nil,
      "POST",
      this.prefix + """books/create/""",
      """Crear y guardar un libro""",
      Seq()
    )
  )

  // @LINE:41
  private[this] lazy val controllers_BooksController_destroy14_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("books/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_BooksController_destroy14_invoker = createInvoker(
    BooksController_2.destroy(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.BooksController",
      "destroy",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """books/delete/""" + "$" + """id<[^/]+>""",
      """Para borrar un libro""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_HomeController_index0_route(params) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_1.index)
      }
  
    // @LINE:8
    case controllers_CountController_count1_route(params) =>
      call { 
        controllers_CountController_count1_invoker.call(CountController_0.count)
      }
  
    // @LINE:10
    case controllers_AsyncController_message2_route(params) =>
      call { 
        controllers_AsyncController_message2_invoker.call(AsyncController_3.message)
      }
  
    // @LINE:13
    case controllers_Assets_versioned3_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned3_invoker.call(Assets_5.versioned(path, file))
      }
  
    // @LINE:16
    case controllers_Services_list4_route(params) =>
      call { 
        controllers_Services_list4_invoker.call(Services_4.list)
      }
  
    // @LINE:17
    case controllers_Services_info5_route(params) =>
      call { 
        controllers_Services_info5_invoker.call(Services_4.info)
      }
  
    // @LINE:18
    case controllers_Services_about6_route(params) =>
      call { 
        controllers_Services_about6_invoker.call(Services_4.about)
      }
  
    // @LINE:19
    case controllers_Services_bienvenido7_route(params) =>
      call(params.fromPath[String]("name", None), params.fromPath[String]("lastName", None)) { (name, lastName) =>
        controllers_Services_bienvenido7_invoker.call(Services_4.bienvenido(name, lastName))
      }
  
    // @LINE:23
    case controllers_BooksController_index8_route(params) =>
      call { 
        controllers_BooksController_index8_invoker.call(BooksController_2.index())
      }
  
    // @LINE:26
    case controllers_BooksController_create9_route(params) =>
      call { 
        controllers_BooksController_create9_invoker.call(BooksController_2.create())
      }
  
    // @LINE:29
    case controllers_BooksController_show10_route(params) =>
      call(params.fromPath[Integer]("id", None)) { (id) =>
        controllers_BooksController_show10_invoker.call(BooksController_2.show(id))
      }
  
    // @LINE:32
    case controllers_BooksController_edit11_route(params) =>
      call(params.fromPath[Integer]("id", None)) { (id) =>
        controllers_BooksController_edit11_invoker.call(BooksController_2.edit(id))
      }
  
    // @LINE:35
    case controllers_BooksController_update12_route(params) =>
      call { 
        controllers_BooksController_update12_invoker.call(BooksController_2.update())
      }
  
    // @LINE:38
    case controllers_BooksController_save13_route(params) =>
      call { 
        controllers_BooksController_save13_invoker.call(BooksController_2.save())
      }
  
    // @LINE:41
    case controllers_BooksController_destroy14_route(params) =>
      call(params.fromPath[Integer]("id", None)) { (id) =>
        controllers_BooksController_destroy14_invoker.call(BooksController_2.destroy(id))
      }
  }
}
